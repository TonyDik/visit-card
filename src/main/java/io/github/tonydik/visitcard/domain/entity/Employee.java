package io.github.tonydik.visitcard.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Employee {
    @Id
    private ObjectId _id;

    private String firstName;
    private String lastName;


    public String get_id() {
        return this._id.toHexString();
    }

}
