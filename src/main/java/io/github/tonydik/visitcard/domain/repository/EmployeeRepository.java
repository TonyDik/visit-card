package io.github.tonydik.visitcard.domain.repository;


import io.github.tonydik.visitcard.domain.entity.Employee;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface EmployeeRepository extends ReactiveMongoRepository<Employee, ObjectId> {
}
