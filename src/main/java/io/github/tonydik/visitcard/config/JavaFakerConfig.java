package io.github.tonydik.visitcard.config;

import com.github.javafaker.Faker;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JavaFakerConfig {

    @Bean
    public Faker getFaker() {
        return new Faker();
    }
}
