package io.github.tonydik.visitcard.services;

import com.github.javafaker.Faker;
import io.github.tonydik.visitcard.domain.entity.Employee;
import io.github.tonydik.visitcard.domain.repository.EmployeeRepository;
import io.github.tonydik.visitcard.dtos.EmployeeDto;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public final class EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final Faker faker;
    private final ModelMapper modelMapper;

    public Flux<EmployeeDto> getAll() {
        return employeeRepository.findAll().map(this::convertToDto);
    }

    public Mono<EmployeeDto> getById(final String id) {
        return employeeRepository.findById(new ObjectId(id)).map(this::convertToDto);
    }

    public Mono<EmployeeDto> create(EmployeeDto employeeDto) {
        return employeeRepository.save(convertToEntity(employeeDto)).map(this::convertToDto);
    }

    public Flux<EmployeeDto> createFakeEmployees(Long number) {
        return employeeRepository.saveAll(generateEmployeeDtoFlux(number).map(this::convertToEntity)).map(this::convertToDto);
    }

    public Mono<Void> deleteAll() {
        return employeeRepository.deleteAll();
    }

    private EmployeeDto generateFakeEmployeeDto() {
        String firstName = faker.name().firstName();
        String lastName = faker.name().lastName();
        return new EmployeeDto().setFirstName(firstName).setLastName(lastName);
    }

    private Flux<EmployeeDto> generateEmployeeDtoFlux(final Long number) {
        List<EmployeeDto> employeeDtos = new ArrayList<>();
        for (long i = 0; i < number; i++) {
            employeeDtos.add(generateFakeEmployeeDto());
        }
        return Flux.fromIterable(employeeDtos);
    }

    private EmployeeDto convertToDto(final Employee employee) {
        return modelMapper.map(employee, EmployeeDto.class).setId(employee.get_id());
    }

    private Employee convertToEntity(final EmployeeDto employeeDto) {
        return modelMapper.map(employeeDto, Employee.class);
    }

}
