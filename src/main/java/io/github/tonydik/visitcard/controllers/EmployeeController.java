package io.github.tonydik.visitcard.controllers;

import io.github.tonydik.visitcard.dtos.EmployeeDto;
import io.github.tonydik.visitcard.services.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/employees")
@RequiredArgsConstructor
public class EmployeeController {

    private final EmployeeService employeeService;

    @GetMapping("/{id}")
    public Mono<EmployeeDto> getById(@PathVariable String id) {
        return employeeService.getById(id);
    }

    @GetMapping
    public Flux<EmployeeDto> getAll() {
        return employeeService.getAll();
    }

    @PostMapping
    public Mono<EmployeeDto> create(@RequestBody EmployeeDto employeeDto) {
        return employeeService.create(employeeDto);
    }

    @GetMapping("/fake/{number}")
    public Flux<EmployeeDto> generateFakeEmployees(@PathVariable Long number) {
        return employeeService.createFakeEmployees(number);
    }

    @DeleteMapping("/all")
    public Mono<Void> deleteAll() {
        return employeeService.deleteAll();
    }

}
