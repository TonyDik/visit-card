package io.github.tonydik.visitcard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VisitCardApplication {

    public static void main(final String[] args) {
        SpringApplication.run(VisitCardApplication.class, args);
    }

}
